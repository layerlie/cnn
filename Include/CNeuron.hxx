#ifndef _NEURON_H_
#define _NEURON_H_

#include <vector>
#include "CNNFunctions.hxx"
#include "CNeuralLink.hxx"

const double LearningRate = 0.01;

namespace CNN
{
	template < typename T >
	class CNeuron
	{
    protected:
        INetworkFunction* _pFunction;

        std::vector<CNeuralLink<T>*> _vInputLinks;
        std::vector<CNeuralLink<T>*> _vOutputLinks;

        double _InputSum;
        
    public:
        CNeuron();
        CNeuron(INetworkFunction* AFunction);
        CNeuron(std::vector<CNeuralLink<T>*> AOutputLinks, INetworkFunction* AFunction);
        CNeuron(std::vector<CNeuron<T>*> AOutputLinks, INetworkFunction* AFunction);

        virtual ~CNeuron();

        virtual std::vector<CNeuralLink<T>*>& GetInputLinks();
        virtual std::vector<CNeuralLink<T>*>& GetOutputLinks();
        virtual int GetNumOfInputLinks() const;
        virtual int GetNumOfLinks() const;
        virtual double GetSum() const;
			
        virtual void AddInputLink(CNeuralLink<T>* ALink);
        virtual void AddOutputLink(CNeuralLink<T>* ALink);

        virtual CNeuralLink<T>* at(const unsigned int AIndex);

        virtual void Input(double AInputData);
        virtual void ResetSum();

        virtual double Fire();

        virtual double Process() const;
        virtual double Process(double AnArgument) const;
        virtual double Derivative() const;
        virtual double Derivative(double AnArgument) const;

        virtual double RunTrainingProcess(double ATarget);
        virtual void RunWeightUpdating();

        virtual void ShowNeuronState();
	};
};

#endif