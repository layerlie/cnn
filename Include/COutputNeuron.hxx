#ifndef _OUTPUT_NEURON_H_
#define _OUTPUT_NEURON_H_

#include "CNeuron.hxx"

namespace CNN
{
	template < typename T >
	class COutputNeuron : public CNeuron < T >
	{
    protected:
        CNeuron<T>* _pNeuron;
        double		_OutputSum;
        
    public:
        explicit COutputNeuron(CNeuron<T>* ANeuron);

        virtual ~COutputNeuron();

        virtual std::vector<CNeuralLink<T>*>& GetInputLinks();
        virtual std::vector<CNeuralLink<T>*>& GetOutputLinks();
        virtual int GetNumOfInputLinks() const;
        virtual int GetNumOfLinks() const;
        virtual double GetSum() const;

        virtual void AddInputLink(CNeuralLink<T>* ALink);
        virtual void AddOutputLink(CNeuralLink<T>* ALink);

        virtual CNeuralLink<T>* at(unsigned int AIndex);

        virtual void Input(double AInputData);
        virtual void ResetSum();

        virtual double Fire();

        virtual double Process() const;
        virtual double Process(double AnArgument) const;
        virtual double Derivative() const;
        virtual double Derivative(double AnArgument) const;

        virtual double RunTrainingProcess(double ATarget);
        virtual void RunWeightUpdating();

        virtual void ShowNeuronState();
	};
};

#endif