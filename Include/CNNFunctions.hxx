#ifndef _NETWORK_FUNCTION_H_
#define _NETWORK_FUNCTION_H_

#include "CNNInterfaces.hxx"
#include <cmath>

namespace CNN
{
	class CLinear : public INetworkFunction
	{
    public:
        CLinear(){};
        virtual ~CLinear(){};
        virtual double Compute(const double& AParam) const { return AParam; };
        virtual double Derivative(const double& AParam) const { return 0; };
	};

	class CBinarySigmoid : public INetworkFunction
	{
    public:
        CBinarySigmoid(){};
        virtual ~CBinarySigmoid(){};
        virtual double Compute(const double& AParam) const { return (1.0 / (1.0 + exp(-AParam))); };
        virtual double Derivative(const double& AParam) const { return (this->Compute(AParam) * (1.0 - this->Compute(AParam))); };
	};

	class CBipolarSigmoid : public INetworkFunction
	{
    public:
        CBipolarSigmoid(){};
        virtual ~CBipolarSigmoid(){};
        virtual double Compute(const double& AParam) const { return (2.0 / (1.0 + exp(-AParam)) - 1.0); };
        virtual double Derivative(const double& AParam) const { return (0.5 * (1.0 + this->Compute(AParam)) * (1.0 - this->Compute(AParam))); };
	};

	class CHyperbolicTangent : public INetworkFunction
	{
    public:
        CHyperbolicTangent(){};
        virtual ~CHyperbolicTangent(){};
        virtual double Compute(const double& AParam) const { return tanh(AParam); };
        virtual double Derivative(const double& AParam) const { return 1.0 / (pow(cosh(AParam), 2)); };
	};
};

#endif