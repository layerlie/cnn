#ifndef _NEURAL_NETWORK_H_
#define _NEURAL_NETWORK_H_

#include "CBackpropagation.hxx"
#include "CNNInterfaces.hxx"
#include "CNeuron.hxx"

namespace CNN
{
	template < typename T >
	class CNeuralNetwork
	{
    protected:
        INeuronFactory<T>*  _NeuronFactory;
        ITrainAlgorithm<T>* _TrainAlgorithm;

        std::vector<CNeuron<T>*> _vBiasLayer;
        std::vector< std::vector<CNeuron<T>*> > _vLayers;

        int _iInputs;
        int _iHiddens;
        int _iOutputs;

        double _MeanSquaredError;
        double _MinMSE;

        std::vector<CNeuron<T>*>& _GetLayer(const int& AIndex);
        std::vector<CNeuron<T>*>& _GetInputLayer();
        std::vector<CNeuron<T>*>& _GetOutputLayer();
        std::vector<CNeuron<T>*>& _GetBiasLayer();

        int size();

        void _UpdateWeights();
        void _ResetCharges();
        void _ResetMSE();

        double _GetMSE();

        void _AddMSE(const double& APortion);

        friend class CBackpropagation < T > ;
        
    public:
        CNeuralNetwork(const int& AInputs,
                       const int& AOutputs,
                       const int& ANumOfHiddenLayers = 0,
                       const int& ANumOfNeuronsInHiddenLayers = 0);

        ~CNeuralNetwork();

        bool Train(const std::vector<std::vector<T> >& AData, const std::vector<std::vector<T> >& ATarget);

        void SetAlgorithm(ITrainAlgorithm<T>* AnAlgorithm);
        void SetNeuronFactory(INeuronFactory<T>* ANeuronFactory);
        void SetMinMSE(const double& AMinMSE);
        void ShowNetworkState();

        const double& GetMinMSE();

        std::vector<int> GetNetworkResponse(const std::vector<T>& AData);
	};
};

#endif