#ifndef _NEURAL_LINK_H_
#define _NEURAL_LINK_H_

namespace CNN
{
	template < typename T >
	class CNeuron;

	template < typename T >
	class CNeuralLink
	{
    private:
        CNeuron<T>* _pNeuron;

        double _LastTranslatedSignal;
        double _ErrorInformation;
        double _WeightCorrection;
        double _WeightOfNeuron;
        
    public:
        CNeuralLink();
        CNeuralLink(CNeuron<T>* ANeuron, double AWeightOfNeuron = 0);

        CNeuron<T>* GetNeuron() const;
        const double& GetWeight() const;
        double GetLastTranslatedSignal() const;
        double GetErrorInformation() const;
        double GetWeightCorrection() const;

        void SetNeuron(CNeuron<T>* ANeuron);
        void SetWeight(const double& AWeight);
        void SetLastTranslatedSignal(const double& ASignal);
        void SetErrorInformation(const double& AErrorInformation);
        void SetWeightCorrection(const double& AWeightCorrection);
        void UpdateWeight();

	};
};

#endif