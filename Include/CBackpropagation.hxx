#ifndef _BACKPROPAGATION_H_
#define _BACKPROPAGATION_H_

#include "CNNInterfaces.hxx"

namespace CNN
{
	template < typename T >
	class CNeuralNetwork;

	template < typename T >
	class CBackpropagation : public ITrainAlgorithm < T >
	{
    protected:
        CNeuralNetwork<T>* _NeuralNetwork;

        void _WeightsInitialization();
        void _CommonInitialization();
        
    public:
        explicit CBackpropagation(CNeuralNetwork<T>* ANeuralNetwork);
        virtual ~CBackpropagation();
        virtual double Train(const std::vector<T>& AData, const std::vector<T>& ATarget);
        virtual void WeightsInitialization();
	};
};

#endif