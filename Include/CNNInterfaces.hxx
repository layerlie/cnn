#ifndef _CNN_INTERFACES_H_
#define _CNN_INTERFACES_H_

#include <vector>

namespace CNN
{
	template < typename T> class CNeuron;

	class INetworkFunction
	{
    public:
        INetworkFunction(){};
        virtual ~INetworkFunction(){};
        virtual double Compute(const double& AParam) const = 0;
        virtual double Derivative(const double& AParam) const = 0;
	};

	template <typename T>
	class INeuronFactory
	{
    public:
        INeuronFactory(){};
        virtual	~INeuronFactory(){};
        virtual CNeuron<T>*	CreateInputNeuron(std::vector<CNeuron<T>*>& ANeuronsLinkTo, INetworkFunction * AFunction) const = 0;
        virtual CNeuron<T>*	CreateHiddenNeuron(std::vector<CNeuron<T>*>& ANeuronsLinkTo, INetworkFunction * AFunction) const = 0;
        virtual CNeuron<T>*	CreateOutputNeuron(INetworkFunction * AFunction) const = 0;
	};

	template <typename T>
	class ITrainAlgorithm
	{
    public:
        virtual ~ITrainAlgorithm(){};
        virtual double Train(const std::vector<T>& AData, const std::vector<T>& ATarget) = 0;
        virtual void WeightsInitialization() = 0;
	};
};

#endif