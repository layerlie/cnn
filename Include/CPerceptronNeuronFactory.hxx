#ifndef _PERCEPTRON_NEURON_FACTORY_H_
#define _PERCEPTRON_NEURON_FACTORY_H_

#include "CNeuron.hxx"

namespace CNN
{
	template <typename T>
	class CPerceptronNeuronFactory : public INeuronFactory<T>
	{
    public:
        CPerceptronNeuronFactory();
        virtual	~CPerceptronNeuronFactory();
        virtual CNeuron<T>*	CreateInputNeuron(std::vector<CNeuron<T>*>&  ANeuronsLinkTo, INetworkFunction* AFunction) const;
        virtual CNeuron<T>*	CreateHiddenNeuron(std::vector<CNeuron<T>*>& ANeuronsLinkTo, INetworkFunction* AFunction) const;
        virtual CNeuron<T>*	CreateOutputNeuron(INetworkFunction* AFunction) const;
	};
};


#endif