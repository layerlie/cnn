#ifndef _HIDDEN_NEURON_H_
#define _HIDDEN_NEURON_H_

#include "CNeuron.hxx"

namespace CNN
{
	template < typename T >
	class CHiddenNeuron : public CNeuron < T >
	{
    protected:
        CNeuron<T>* _pNeuron;
        
    public:
        explicit CHiddenNeuron(CNeuron<T>* ANeuron);

        virtual ~CHiddenNeuron();

        virtual std::vector<CNeuralLink<T>*>& GetInputLinks();
        virtual std::vector<CNeuralLink<T>*>& GetOutputLinks();
        virtual int GetNumOfInputLinks() const;
        virtual int GetNumOfLinks() const;
        virtual double GetSum() const;

        virtual void AddInputLink(CNeuralLink<T>* ALink);
        virtual void AddOutputLink(CNeuralLink<T>* ALink);

        virtual CNeuralLink<T>* at(unsigned int AIndex);

        virtual void Input(double AInputData);
        virtual void ResetSum();

        virtual double Fire();

        virtual double Process() const;
        virtual double Process(double AnArgument) const;
        virtual double Derivative() const;
        virtual double Derivative(double AnArgument) const;

        virtual double RunTrainingProcess(double ATarget);
        virtual void RunWeightUpdating();

        virtual void ShowNeuronState();
	};
};

#endif