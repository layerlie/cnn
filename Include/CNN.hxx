#ifndef _CROSSPLATFORM_NEURAL_NETWORK_H_
#define _CROSSPLATFORM_NEURAL_NETWORK_H_

#include "CPerceptronNeuronFactory.hxx"
#include "CBackpropagation.hxx"
#include "CNeuralNetwork.hxx"
#include "CNNFunctions.hxx"
#include "CNeuron.hxx"
#include <iostream>
#include <locale>
#include <vector>

#define _USE_MATH_DEFINES

#include <math.h>

#endif