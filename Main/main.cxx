#include "CNN.hxx"

using namespace CNN;
using namespace std;

const float MIN_MSE = 0.01;

int main(int argc, char** argv)
{
	setlocale(LC_ALL, "Russian");
	
	cout.precision(6);
	cout.setf(ios::fixed, ios::floatfield);

	vector< vector<double> > Data;

	vector<double> tempData1;
	tempData1.push_back(1.0);
	tempData1.push_back(1.0);

	vector<double> tempData2;
	tempData2.push_back(1.0);
	tempData2.push_back(-1.0);

	vector<double> tempData3;
	tempData3.push_back(-1.0);
	tempData3.push_back(1.0);

	vector<double> tempData4;
	tempData4.push_back(-1.0);
	tempData4.push_back(-1.0);

	Data.push_back(tempData1);
	Data.push_back(tempData2);
	Data.push_back(tempData3);
	Data.push_back(tempData4);

	vector< vector<double> > TrainingExample;

	float Sin1;
	float Sin2;
	float Sin3;
	float Sin4;

	Sin1 = sin(M_PI / 2.0);
	vector<double> sin1;
	sin1.push_back(Sin1);

	Sin2 = sin(M_PI / 3.0);
	vector<double> sin2;
	sin2.push_back(Sin2);

	Sin3 = sin(M_PI / 4.0);
	vector<double> sin3;
	sin3.push_back(Sin3);

	Sin4 = sin(M_PI / 6.0);
	vector<double> sin4;
	sin4.push_back(Sin4);

	TrainingExample.push_back(sin1);
	TrainingExample.push_back(sin2);
	TrainingExample.push_back(sin3);
	TrainingExample.push_back(sin4);
    
	CNeuralNetwork<double>* NeuralNetwork = new CNeuralNetwork<double>(2, 1, 1, 4);

	NeuralNetwork->SetMinMSE(MIN_MSE);
	NeuralNetwork->Train(Data, TrainingExample);

	std::cout << "\nInput data: { 1, 1 } "   << "\nExpected value: " << Sin1 << '\n';
	NeuralNetwork->GetNetworkResponse(Data[0]);

    std::cout << "\nInput data: { 1, -1 } "  << "\nExpected value: " << Sin2 << '\n';
	NeuralNetwork->GetNetworkResponse(Data[1]);

    std::cout << "\nInput data: { -1, 1 } "  << "\nExpected value: " << Sin3 << '\n';
	NeuralNetwork->GetNetworkResponse(Data[2]);

    std::cout << "\nInput data: { -1, -1 } " << "\nExpected value: " << Sin4 << '\n';
	NeuralNetwork->GetNetworkResponse(Data[3]);

	delete NeuralNetwork;

	return 0;
};