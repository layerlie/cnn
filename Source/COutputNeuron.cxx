#include "COutputNeuron.hxx"

namespace CNN
{
	template < typename T >
	COutputNeuron<T>::~COutputNeuron()
	{
		if (_pNeuron)
			delete _pNeuron;
	};

	template < typename T >
	COutputNeuron<T>::COutputNeuron(CNeuron<T>* ANeuron)
	{
		_OutputSum	= 0;
		_pNeuron	= ANeuron;
	};

	template < typename T >
	std::vector<CNeuralLink<T>*>& COutputNeuron<T>::GetInputLinks()
	{
		return _pNeuron->GetInputLinks();
	};

	template < typename T >
	std::vector<CNeuralLink<T>*>& COutputNeuron<T>::GetOutputLinks()
	{
		return _pNeuron->GetOutputLinks();
	};

	template < typename T >
	int COutputNeuron<T>::GetNumOfInputLinks() const
	{
		return _pNeuron->GetNumOfInputLinks();
	};

	template < typename T >
	int COutputNeuron<T>::GetNumOfLinks() const
	{
		return _pNeuron->GetNumOfLinks();
	};

	template < typename T >
	double COutputNeuron<T>::GetSum() const
	{
		return _pNeuron->GetSum();
	};

	template < typename T >
	void COutputNeuron<T>::AddInputLink(CNeuralLink<T>* ALink)
	{
		_pNeuron->AddInputLink(ALink);
	};

	template < typename T >
	void COutputNeuron<T>::AddOutputLink(CNeuralLink<T>* ALink)
	{
		_pNeuron->AddOutputLink(ALink);
	};

	template < typename T >
	CNeuralLink<T>* COutputNeuron<T>::at(unsigned int AIndex)
	{
		return _pNeuron->at(AIndex);
	};

	template < typename T >
	void COutputNeuron<T>::Input(double AInputData)
	{
		_pNeuron->Input(AInputData);
	};

	template < typename T >
	void COutputNeuron<T>::ResetSum()
	{
		_pNeuron->ResetSum();
	};

	template < typename T >
	double COutputNeuron<T>::Fire()
	{
		double output	= this->Process();
		_OutputSum		= output;

		return output;
	};

	template < typename T >
	double COutputNeuron<T>::Process() const
	{
		return _pNeuron->Process();
	};

	template < typename T >
	double COutputNeuron<T>::Process(double AnArgument) const
	{
		return _pNeuron->Process(AnArgument);
	};

	template < typename T >
	double COutputNeuron<T>::Derivative() const
	{
		return _pNeuron->Derivative();
	};

	template < typename T >
	double COutputNeuron<T>::Derivative(double AnArgument) const
	{
		return _pNeuron->Derivative(AnArgument);
	};

	template < typename T >
	double COutputNeuron<T>::RunTrainingProcess(double ATarget)
	{
        double err  = (ATarget - _OutputSum) * _pNeuron->Derivative();
        double res  = pow(ATarget - _OutputSum, 2);

		for (int i = 0; i < (this->GetInputLinks()).size(); i++)
		{
            CNeuralLink<T> * pInputLink = (this->GetInputLinks()).at(i);
            double z                    = pInputLink->GetLastTranslatedSignal();
            double WeightCorrectionTerm = z * err;

			pInputLink->SetWeightCorrection(LearningRate * WeightCorrectionTerm);

			pInputLink->SetErrorInformation(err);
		};


		return res;
	};

	template < typename T >
	void COutputNeuron<T>::RunWeightUpdating()
	{
		for (int i = 0; i < this->GetInputLinks().size(); i++)
		{
			this->GetInputLinks().at(i)->UpdateWeight();
		};
	};

	template < typename T >
	void COutputNeuron<T>::ShowNeuronState()
	{
		_pNeuron->ShowNeuronState();
	};

	template class COutputNeuron < double >;
	template class COutputNeuron < float >;
	template class COutputNeuron < int >;
};