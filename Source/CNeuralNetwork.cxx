#include "CPerceptronNeuronFactory.hxx"
#include "CNeuralNetwork.hxx"
#include <iostream>

namespace CNN
{
	template < typename T >
    CNeuralNetwork<T>::CNeuralNetwork(const int& AInputs,
                                      const int& AOutputs,
                                      const int& ANumOfHiddenLayers,
                                      const int& ANumOfNeuronsInHiddenLayers)
	{
		if (AInputs > 0 && AOutputs > 0)
		{
			_MinMSE             = 0.01;
			_MeanSquaredError   = 0;
			_iInputs            = AInputs;
			_iOutputs           = AOutputs;
			_iHiddens           = ANumOfNeuronsInHiddenLayers;

			INetworkFunction* outputNeuronFunc;
			INetworkFunction* inputNeuronFunc;

			std::vector<CNeuron<T>*> outputLayer;
			std::vector<CNeuron<T>*> inputLayer;

			_NeuronFactory	= new CPerceptronNeuronFactory < T > ;
			_TrainAlgorithm = new CBackpropagation<T>(this);

			outputNeuronFunc = new CBipolarSigmoid;
			inputNeuronFunc  = new CLinear;

			for (int iNumOfOutputs = 0; iNumOfOutputs < AOutputs; iNumOfOutputs++)
				outputLayer.push_back(_NeuronFactory->CreateOutputNeuron(outputNeuronFunc));

			_vLayers.push_back(outputLayer);

			for (int i = 0; i < ANumOfHiddenLayers; i++)
			{
				std::vector<CNeuron<T>*> hiddenLayer;
				for (int j = 0; j < ANumOfNeuronsInHiddenLayers; j++)
					hiddenLayer.push_back(_NeuronFactory->CreateHiddenNeuron(_vLayers[0], outputNeuronFunc));
				_vBiasLayer.insert(_vBiasLayer.begin(), _NeuronFactory->CreateInputNeuron(_vLayers[0], inputNeuronFunc));
				_vLayers.insert(_vLayers.begin(), hiddenLayer);
			};

			for (int iNumOfInputs = 0; iNumOfInputs < AInputs; iNumOfInputs++)
				inputLayer.push_back(_NeuronFactory->CreateInputNeuron(_vLayers[0], inputNeuronFunc));

			_vBiasLayer.insert(_vBiasLayer.begin(), _NeuronFactory->CreateInputNeuron(_vLayers[0], inputNeuronFunc));
			_vLayers.insert(_vLayers.begin(), inputLayer);

			_TrainAlgorithm->WeightsInitialization();
		};
	};

	template < typename T >
	CNeuralNetwork<T>::~CNeuralNetwork()
	{
        
	};

	template < typename T >
	std::vector<CNeuron<T>*>& CNeuralNetwork<T>::_GetLayer(const int& AIndex)
	{
		return _vLayers[AIndex];
	};

	template < typename T >
	std::vector<CNeuron<T>*>& CNeuralNetwork<T>::_GetBiasLayer()
	{
		return _vBiasLayer;
	};

	template < typename T >
	std::vector<CNeuron<T>*>& CNeuralNetwork<T>::_GetInputLayer()
	{
		return _vLayers[0];
	};

	template < typename T >
	std::vector<CNeuron<T>*>& CNeuralNetwork<T>::_GetOutputLayer()
	{
		return _vLayers[_vLayers.size() - 1];
	};

	template < typename T >
	int CNeuralNetwork<T>::size()
	{
		return _vLayers.size();
	};

	template < typename T >
	void CNeuralNetwork<T>::_AddMSE(const double& APortion)
	{
		_MeanSquaredError += APortion;
	};

	template < typename T >
	void CNeuralNetwork<T>::_UpdateWeights()
	{
		for (int i = 0; i < _vLayers.size(); i++)
			for (int j = 0; j < _vLayers.at(i).size(); j++)
				_vLayers.at(i).at(j)->RunWeightUpdating();
	};

	template < typename T >
	void CNeuralNetwork<T>::_ResetCharges()
	{
		for (int i = 0; i < _vLayers.size(); i++)
			for (int j = 0; j < _vLayers.at(i).size(); j++)
				_vLayers.at(i).at(j)->ResetSum();

		for (int i = 0; i < _vLayers.size() - 1; i++)
			_vBiasLayer[i]->ResetSum();
	};

	template < typename T >
	void CNeuralNetwork<T>::_ResetMSE()
	{
		_MeanSquaredError = 0;
	};

	template < typename T >
	double CNeuralNetwork<T>::_GetMSE()
	{
		return _MeanSquaredError;
	};

	template < typename T >
	bool CNeuralNetwork<T>::Train(const std::vector<std::vector<T> >& AData, const std::vector<std::vector<T> >& ATarget)
	{
		bool active = true;
		int IterationsCount = 0;
		while (active)
		{
			IterationsCount++;

			for (int i = 0; i < AData.size(); i++)
				_TrainAlgorithm->Train(AData[i], ATarget[i]);

			double MSE = this->_GetMSE();

			if (MSE < _MinMSE)
			{
				std::cout << IterationsCount << " iterations\n";
				std::cout << "MSE = " << MSE << '\n';
				active = false;
			};
			
			this->_ResetMSE();
		};

		return active;
	};

	template < typename T >
	void CNeuralNetwork<T>::SetAlgorithm(ITrainAlgorithm<T>* AnAlgorithm)
	{
		_TrainAlgorithm = AnAlgorithm;
	};

	template < typename T >
	void CNeuralNetwork<T>::SetNeuronFactory(INeuronFactory<T>* ANeuronFactory)
	{
		_NeuronFactory = ANeuronFactory;
	};

	template < typename T >
	void CNeuralNetwork<T>::SetMinMSE(const double& AMinMSE)
	{
		_MinMSE = AMinMSE;
	};

	template < typename T >
	void CNeuralNetwork<T>::ShowNetworkState()
	{
		std::cout << '\n';

		for (int i = 0; i < _vLayers.size(); i++)
		{
			std::cout << "Layer index = " << i << '\n';
			
			for (int j = 0; j < _vLayers[i].size(); j++)
			{
				std::cout << "Neuron index: " << j << '\n';
				_vLayers[i][j]->ShowNeuronState();
			};
			
			if (i < _vBiasLayer.size())
			{
				std::cout << "Bias: \n";
				_vBiasLayer[i]->ShowNeuronState();
			};
		};
	};

	template < typename T >
	const double& CNeuralNetwork<T>::GetMinMSE()
	{
		return _MinMSE;
	};

	template < typename T >
	std::vector<int> CNeuralNetwork<T>::GetNetworkResponse(const std::vector<T>& AData)
	{
		std::vector<int> vNetworkResponse;

		if (AData.size() != _iInputs)
		{
			std::cout << "Input data dimensions are wrong, expected: " << _iInputs << " elements\n";
			return vNetworkResponse;
		}
		else
		{
			for (int i = 0; i < this->_GetInputLayer().size(); i++)
				this->_GetInputLayer().at(i)->Input(AData[i]);

			for (int i = 0; i < _vLayers.size() - 1; i++)
			{
				_vBiasLayer[i]->Input(1.0);
				
				for (int j = 0; j < _vLayers.at(i).size(); j++)
					_vLayers.at(i).at(j)->Fire();

				_vBiasLayer[i]->Fire();
			};

			std::cout << "Network Response is { ";

			for (int i = 0; i < _iOutputs; i++)
			{
				double res = this->_GetOutputLayer().at(i)->Fire();
				std::cout << "Res: " << res;
			};

			std::cout << " }\n";

			this->_ResetCharges();
			return vNetworkResponse;
		};
	};

	template class CNeuralNetwork < int > ;
	template class CNeuralNetwork < float >;
	template class CNeuralNetwork < double >;
};