#include "CPerceptronNeuronFactory.hxx"
#include "COutputNeuron.hxx"
#include "CHiddenNeuron.hxx"

namespace CNN
{
	template < typename T >
	CPerceptronNeuronFactory<T>::CPerceptronNeuronFactory()
	{

	};

	template < typename T >
	CPerceptronNeuronFactory<T>::~CPerceptronNeuronFactory()
	{

	};

	template < typename T >
	CNeuron<T>* CPerceptronNeuronFactory<T>::CreateInputNeuron(std::vector<CNeuron<T>*>& ANeuronsLinkTo, INetworkFunction* AFunction) const
	{
		return new CNeuron<T>(ANeuronsLinkTo, AFunction);
	};

	template < typename T >
	CNeuron<T>* CPerceptronNeuronFactory<T>::CreateOutputNeuron(INetworkFunction* AFunction) const
	{
		return new COutputNeuron<T>(new CNeuron<T>(AFunction));
	};

	template < typename T >
	CNeuron<T>* CPerceptronNeuronFactory<T>::CreateHiddenNeuron(std::vector<CNeuron<T>*>& ANeuronsLinkTo, INetworkFunction* AFunction) const
	{
		return new CHiddenNeuron<T>(new CNeuron<T>(ANeuronsLinkTo, AFunction));
	};

	template class CPerceptronNeuronFactory < int > ;
	template class CPerceptronNeuronFactory < float >;
	template class CPerceptronNeuronFactory < double >;
};