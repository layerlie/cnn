#include "CHiddenNeuron.hxx"

namespace CNN
{
	template < typename T >
	CHiddenNeuron<T>::~CHiddenNeuron()
	{
		if (_pNeuron)
			delete _pNeuron;
	};

	template < typename T >
	CHiddenNeuron<T>::CHiddenNeuron(CNeuron<T>* ANeuron)
	{
		_pNeuron = ANeuron;
	};

	template < typename T >
	std::vector<CNeuralLink<T>*>& CHiddenNeuron<T>::GetInputLinks()
	{
		return _pNeuron->GetInputLinks();
	};

	template < typename T >
	std::vector<CNeuralLink<T>*>& CHiddenNeuron<T>::GetOutputLinks()
	{
		return _pNeuron->GetOutputLinks();
	};

	template < typename T >
	int CHiddenNeuron<T>::GetNumOfInputLinks() const
	{
		return _pNeuron->GetNumOfInputLinks();
	};

	template < typename T >
	int CHiddenNeuron<T>::GetNumOfLinks() const
	{
		return _pNeuron->GetNumOfLinks();
	};

	template < typename T >
	double CHiddenNeuron<T>::GetSum() const
	{
		return _pNeuron->GetSum();
	};

	template < typename T >
	void CHiddenNeuron<T>::AddInputLink(CNeuralLink<T>* ALink)
	{
		_pNeuron->AddInputLink(ALink);
	};

	template < typename T >
	void CHiddenNeuron<T>::AddOutputLink(CNeuralLink<T>* ALink)
	{
		_pNeuron->AddOutputLink(ALink);
	};

	template < typename T >
	CNeuralLink<T>* CHiddenNeuron<T>::at(unsigned int AIndex)
	{
		return _pNeuron->at(AIndex);
	};

	template < typename T >
	void CHiddenNeuron<T>::Input(double AInputData)
	{
		_pNeuron->Input(AInputData);
	};

	template < typename T >
	void CHiddenNeuron<T>::ResetSum()
	{
		_pNeuron->ResetSum();
	};

	template < typename T >
	double CHiddenNeuron<T>::Fire()
	{
		for (int i = 0; i < this->GetNumOfLinks(); i++)
		{
            CNeuralLink<T>* link = _pNeuron->at(i);
            CNeuron<T>* neuron	 = link->GetNeuron();
            double w             = link->GetWeight();
            double charge        = _pNeuron->GetSum();
            double z             = _pNeuron->Process(charge);
            double output        = z * w;

			link->SetLastTranslatedSignal(z);
			neuron->Input(output);
		};

		return _pNeuron->GetSum();
	};

	template < typename T >
	double CHiddenNeuron<T>::Process() const
	{
		return _pNeuron->Process();
	};

	template < typename T >
	double CHiddenNeuron<T>::Process(double AnArgument) const
	{
		return _pNeuron->Process(AnArgument);
	};

	template < typename T >
	double CHiddenNeuron<T>::Derivative() const
	{
		return _pNeuron->Derivative();
	};

	template < typename T >
	double CHiddenNeuron<T>::Derivative(double AnArgument) const
	{
		return _pNeuron->Derivative(AnArgument);
	};

	template < typename T >
	double CHiddenNeuron<T>::RunTrainingProcess(double ATarget)
	{
		double DeltaInputs = 0;
		for (int i = 0; i < (this->GetNumOfLinks()); i++)
		{
            CNeuralLink<T>* link        = (this->GetOutputLinks()).at(i);
            double ErrorInformationTerm = link->GetErrorInformation();
            double Weight               = link->GetWeight();
            DeltaInputs                 = DeltaInputs + (Weight * ErrorInformationTerm);
		};

		double err = DeltaInputs * (this->Derivative());
		
		for (int i = 0; i < (this->GetInputLinks()).size(); i++)
		{
            CNeuralLink<T>* pInputLink  = (this->GetInputLinks()).at(i);
            double x                    = pInputLink->GetLastTranslatedSignal();
            double WeightCorrectionTerm = x * err;

			pInputLink->SetWeightCorrection(LearningRate * WeightCorrectionTerm);
			
			pInputLink->SetErrorInformation(err);
		};

		return 0;
	};

	template < typename T >
	void CHiddenNeuron<T>::RunWeightUpdating()
	{
		for (int i = 0; i < this->GetInputLinks().size(); i++)
		{
			this->GetInputLinks().at(i)->UpdateWeight();
		};
	};

	template < typename T >
	void CHiddenNeuron<T>::ShowNeuronState()
	{
		_pNeuron->ShowNeuronState();
	};

	template class CHiddenNeuron < double >;
	template class CHiddenNeuron < float >;
	template class CHiddenNeuron < int >;
};