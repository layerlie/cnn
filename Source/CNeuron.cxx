#include "CNeuron.hxx"
#include <iostream>
#include <iomanip>

namespace CNN
{
	template < typename T >
	CNeuron<T>::~CNeuron()
	{
		if (_pFunction)
			delete _pFunction;
	};

	template < typename T >
	CNeuron<T>::CNeuron()
	{
        _InputSum   = 0;
        _pFunction  = new CLinear;
	};

	template < typename T >
	CNeuron<T>::CNeuron(INetworkFunction* AFunction)
	{
        _InputSum   = 0;
        _pFunction  = AFunction;
	};

	template < typename T >
	CNeuron<T>::CNeuron(std::vector<CNeuralLink<T>*> AOutputLinks, INetworkFunction* AFunction)
	{
        _vOutputLinks   = AOutputLinks;
        _pFunction      = AFunction;
        _InputSum       = 0;
	};

	template < typename T >
	CNeuron<T>::CNeuron(std::vector<CNeuron<T>*> AOutputLinks, INetworkFunction* AFunction)
	{
        _pFunction  = AFunction;
        _InputSum   = 0.0;

		for (int i = 0; i < AOutputLinks.size(); i++)
		{
			CNeuralLink<T>* link = new CNeuralLink<T>(AOutputLinks[i], 0.0);

			_vOutputLinks.push_back(link);

			AOutputLinks[i]->AddInputLink(link);
		};
	};

	template < typename T >
	std::vector<CNeuralLink<T>*>& CNeuron<T>::GetInputLinks()
	{
		return _vInputLinks;
	};

	template < typename T >
	std::vector<CNeuralLink<T>*>& CNeuron<T>::GetOutputLinks()
	{
		return _vOutputLinks;
	};

	template < typename T >
	int CNeuron<T>::GetNumOfInputLinks() const
	{
		return _vInputLinks.size();
	};

	template < typename T >
	int CNeuron<T>::GetNumOfLinks() const
	{
		return _vOutputLinks.size();
	};

	template < typename T >
	double CNeuron<T>::GetSum() const
	{
		return _InputSum;
	};

	template < typename T >
	void CNeuron<T>::AddInputLink(CNeuralLink<T>* ALink)
	{
		_vInputLinks.push_back(ALink);
	};

	template < typename T >
	void CNeuron<T>::AddOutputLink(CNeuralLink<T>* ALink)
	{
		_vOutputLinks.push_back(ALink);
	};

	template < typename T >
	CNeuralLink<T>* CNeuron<T>::at(unsigned int AIndex)
	{
		return _vOutputLinks[AIndex];
	};

	template < typename T >
	void CNeuron<T>::Input(double AInputData)
	{
		_InputSum += AInputData;
	};

	template < typename T >
	void CNeuron<T>::ResetSum()
	{
		_InputSum = 0;
	};

	template < typename T >
	double CNeuron<T>::Fire()
	{
		for (int i = 0; i < this->GetNumOfLinks(); i++)
		{
            CNeuralLink<T>* link    = _vOutputLinks[i];
            CNeuron<T>* neuron      = link->GetNeuron();

            double w        = link->GetWeight();
            double charge   = _InputSum;
            double x        = _pFunction->Compute(charge);
            double output   = w * x;

			link->SetLastTranslatedSignal(x);
			neuron->Input(output);
		};

		return _InputSum;
	};

	template < typename T >
	double CNeuron<T>::Process() const
	{
		return _pFunction->Compute(_InputSum);
	};

	template < typename T >
	double CNeuron<T>::Process(double AnArgument) const
	{
		return _pFunction->Compute(AnArgument);
	};

	template < typename T >
	double CNeuron<T>::Derivative() const
	{
		return _pFunction->Derivative(_InputSum);
	};

	template < typename T >
	double CNeuron<T>::Derivative(double AnArgument) const
	{
		return _pFunction->Derivative(AnArgument);
	};

	template < typename T >
	double CNeuron<T>::RunTrainingProcess(double ATarget)
	{
		return 0;
	};

	template < typename T >
	void CNeuron<T>::RunWeightUpdating()
	{

	};

	template < typename T >
	void CNeuron<T>::ShowNeuronState()
	{
		for (int i = 0; i < _vOutputLinks.size(); i++)
		{
            CNeuralLink<T>* pCurrentLink = _vOutputLinks.at(i);
            
            std::cout << "Index of link: "  << std::setw(3) << i + 1 << ' ';
            std::cout << "Weight: "         << std::setw(7) << pCurrentLink->GetWeight();
            std::cout << "; Charge: "       << std::setw(7) <<  pCurrentLink->GetNeuron()->GetSum();
            std::cout << '\n';
		};
	};

	template class CNeuron < double > ;
	template class CNeuron < float >;
	template class CNeuron < int >;
};