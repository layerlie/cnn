#include "CBackpropagation.hxx"
#include "CNeuralNetwork.hxx"
#include <iostream>
#include <cstdlib>
#include <ctime>

namespace CNN
{
	template < typename T >
	CBackpropagation<T>::CBackpropagation(CNeuralNetwork<T>* ANeuralNetwork)
	{
		_NeuralNetwork = ANeuralNetwork;
	};

	template < typename T >
	CBackpropagation<T>::~CBackpropagation()
	{
		if (_NeuralNetwork)
			delete _NeuralNetwork;
	};

	template < typename T >
	void CBackpropagation<T>::_WeightsInitialization()
	{
		srand((unsigned)time(0));

		double InputsCount  = _NeuralNetwork->_iInputs;
		double HiddensCount = _NeuralNetwork->_iHiddens;
		double degree       = 1.0 / InputsCount;
		double ScaleFactor  = 0.7 * (pow(HiddensCount, degree));

		for (int iLayerIndex = 0; iLayerIndex < _NeuralNetwork->size(); iLayerIndex++)
		{
			for (int iNeuronIndex = 0; iNeuronIndex < _NeuralNetwork->_GetLayer(iLayerIndex).size(); iNeuronIndex++)
			{
				CNeuron<T>* currentNeuron = _NeuralNetwork->_GetLayer(iLayerIndex).at(iNeuronIndex);

				for (int iLinkIndex = 0; iLinkIndex < currentNeuron->GetNumOfLinks(); iLinkIndex++)	
				{
					CNeuralLink<T>* currentLink = currentNeuron->at(iLinkIndex);
					float pseudoRandomWeight    = -0.5 + (float)rand() / ((float)RAND_MAX / (0.5 + 0.5));
                    
					currentLink->SetWeight(pseudoRandomWeight);
				};
			};
		};

		for (int iHiddenNeuronIndex = 0; iHiddenNeuronIndex < _NeuralNetwork->_GetLayer(1).size(); iHiddenNeuronIndex++)
		{
			double squaredNorm = 0;

			for (int iInputNeuronIndex = 0; iInputNeuronIndex < _NeuralNetwork->_GetLayer(0).size(); iInputNeuronIndex++)
			{
				CNeuron<T>* currentNeuron   = _NeuralNetwork->_GetLayer(0).at(iInputNeuronIndex);
				CNeuralLink<T>* currentLink = currentNeuron->at(iHiddenNeuronIndex);
                
				squaredNorm += pow(currentLink->GetWeight(), 2.0);
			};

			double norm = sqrt(squaredNorm);

			for (int iInputNeuronIndex = 0; iInputNeuronIndex < _NeuralNetwork->_GetLayer(0).size(); iInputNeuronIndex++)
			{
				CNeuron<T>* currentNeuron   = _NeuralNetwork->_GetLayer(0).at(iInputNeuronIndex);
				CNeuralLink<T>* currentLink = currentNeuron->at(iHiddenNeuronIndex);
				double newWeight            = (ScaleFactor * (currentLink->GetWeight())) / norm;
				
                currentLink->SetWeight(newWeight);
			};
		};

		for (int iBiasLayerIndex = 0; iBiasLayerIndex < _NeuralNetwork->size() - 1; iBiasLayerIndex++)
		{
			CNeuron<T>* bias = _NeuralNetwork->_GetBiasLayer().at(iBiasLayerIndex);

			for (int iLinkIndex = 0; iLinkIndex < bias->GetNumOfLinks(); iLinkIndex++)
			{
				CNeuralLink<T>* currentLink = bias->at(iLinkIndex);

				float pseudoRandomWeight    = -ScaleFactor + (float)rand() / ((float)RAND_MAX / (ScaleFactor + ScaleFactor));

				currentLink->SetWeight(pseudoRandomWeight);
			};
		};
	};

	template < typename T >
	void CBackpropagation<T>::_CommonInitialization()
	{
		srand((unsigned)time(0));

		for (int iLayerIndex = 0; iLayerIndex < _NeuralNetwork->size(); iLayerIndex++)
		{
			for (int iNeuronIndex = 0; iNeuronIndex < _NeuralNetwork->_GetLayer(iLayerIndex).size(); iNeuronIndex++)
			{
				CNeuron<T>* currentNeuron = _NeuralNetwork->_GetLayer(iLayerIndex).at(iNeuronIndex);

				for (int iLinkIndex = 0; iLinkIndex < currentNeuron->GetNumOfInputLinks(); iLinkIndex++)
				{
					CNeuralLink<T>* currentLink = currentNeuron->at(iLinkIndex);
					float pseudoRandomWeight    = -0.5 + (float)rand() / ((float)RAND_MAX / (0.5 + 0.5));
                    
					currentLink->SetWeight(pseudoRandomWeight);
				};
			};
		};

		for (int iBiasLayerIndex = 0; iBiasLayerIndex < _NeuralNetwork->size() - 1; iBiasLayerIndex++)
		{
			CNeuron<T>* bias = _NeuralNetwork->_GetBiasLayer().at(iBiasLayerIndex);

			for (int iLinkIndex = 0; iLinkIndex < bias->GetNumOfLinks(); iLinkIndex++)
			{
				CNeuralLink<T>* currentLink = bias->at(iLinkIndex);
				float pseudoRandomWeight    = -0.5 + (float)rand() / ((float)RAND_MAX / (0.5 + 0.5));

				currentLink->SetWeight(pseudoRandomWeight);
			};
		};
	};

	template < typename T >
	void CBackpropagation<T>::WeightsInitialization()
	{
		this->_WeightsInitialization();
	};

	template < typename T >
	double CBackpropagation<T>::Train(const std::vector<T>& AData, const std::vector<T>& ATarget)
	{
		double result = 0;

		if (AData.size() != _NeuralNetwork->_iInputs || ATarget.size() != _NeuralNetwork->_iOutputs)
		{
			std::cout << "Input data dimensions are wrong, expected: " << _NeuralNetwork->_iInputs << " elements\n";
			return -1;
		}
		else
		{
			for (int i = 0; i < _NeuralNetwork->_iInputs; i++)
				_NeuralNetwork->_GetInputLayer().at(i)->Input(AData[i]);

			for (int i = 0; i < _NeuralNetwork->size() - 1; i++)
			{
				_NeuralNetwork->_GetBiasLayer().at(i)->Input(1.0);
				for (int j = 0; j < _NeuralNetwork->_GetLayer(i).size(); j++)
					_NeuralNetwork->_GetLayer(i).at(j)->Fire();

				_NeuralNetwork->_GetBiasLayer().at(i)->Fire();

				for (int j = 0; j < _NeuralNetwork->_GetBiasLayer().at(i)->GetNumOfLinks(); j++)
					_NeuralNetwork->_GetBiasLayer().at(i)->GetOutputLinks().at(j)->SetLastTranslatedSignal(1);
			};

			std::vector<double> vNetworkResponse;
			for (int iOutputNeuronIndex = 0; iOutputNeuronIndex < _NeuralNetwork->_iOutputs; iOutputNeuronIndex++)
			{
				double Y = _NeuralNetwork->_GetOutputLayer().at(iOutputNeuronIndex)->Fire();
				vNetworkResponse.push_back(Y);
			};

			for (int i = 0; i < _NeuralNetwork->_iOutputs; i++)
			{
				result = _NeuralNetwork->_GetOutputLayer().at(i)->RunTrainingProcess(ATarget[i]);
				_NeuralNetwork->_AddMSE(result);
			};

			for (int iLayerIndex = _NeuralNetwork->size() - 2; iLayerIndex > 0; iLayerIndex--)
			{
				for (int iNeuronIndex = 0; iNeuronIndex < _NeuralNetwork->_GetLayer(iLayerIndex).size(); iNeuronIndex++)
				{
					_NeuralNetwork->_GetLayer(iLayerIndex).at(iNeuronIndex)->RunTrainingProcess(0);
				};
			};

			_NeuralNetwork->_UpdateWeights();
			_NeuralNetwork->_ResetCharges();

			return result;
		};

	};

	template class CBackpropagation < int > ;
	template class CBackpropagation < float >;
	template class CBackpropagation < double >;
};