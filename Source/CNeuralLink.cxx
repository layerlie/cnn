#include "CNeuralLink.hxx"
#include "CNeuron.hxx"

namespace CNN
{
	template < typename T >
	CNeuralLink<T>::CNeuralLink()
	{
        _LastTranslatedSignal   = 0;
        _ErrorInformation       = 0;
        _WeightCorrection       = 0;
        _WeightOfNeuron         = 0;
        _pNeuron                = nullptr;
	};

	template < typename T >
	CNeuralLink<T>::CNeuralLink(CNeuron<T>* ANeuron, double AWeightOfNeuron)
	{
        _pNeuron                = ANeuron;
        _WeightOfNeuron         = AWeightOfNeuron;
        _LastTranslatedSignal   = 0;
        _ErrorInformation       = 0;
        _WeightCorrection       = 0;

	};

	template < typename T >
	CNeuron<T>* CNeuralLink<T>::GetNeuron() const
	{
		return _pNeuron;
	};

	template < typename T >
	const double& CNeuralLink<T>::GetWeight() const
	{
		return _WeightOfNeuron;
	};

	template < typename T >
	double CNeuralLink<T>::GetLastTranslatedSignal() const
	{
		return _LastTranslatedSignal;
	};

	template < typename T >
	double CNeuralLink<T>::GetErrorInformation() const
	{
		return _ErrorInformation;
	};

	template < typename T >
	double CNeuralLink<T>::GetWeightCorrection() const
	{
		return _WeightCorrection;
	};

	template < typename T >
	void CNeuralLink<T>::SetNeuron(CNeuron<T>* ANeuron)
	{
		_pNeuron = ANeuron;
	};

	template < typename T >
	void CNeuralLink<T>::SetWeight(const double& AWeight)
	{
		_WeightOfNeuron = AWeight;
	};

	template < typename T >
	void CNeuralLink<T>::SetLastTranslatedSignal(const double& ASignal)
	{
		_LastTranslatedSignal = ASignal;
	};

	template < typename T >
	void CNeuralLink<T>::SetErrorInformation(const double& AErrorInformation)
	{
		_ErrorInformation = AErrorInformation;
	};

	template < typename T >
	void CNeuralLink<T>::SetWeightCorrection(const double& AWeightCorrection)
	{
		_WeightCorrection = AWeightCorrection;
	};

	template < typename T >
	void CNeuralLink<T>::UpdateWeight()
	{
		_WeightOfNeuron = _WeightOfNeuron + _WeightCorrection;
	};
	
	template class CNeuralLink < double > ;
	template class CNeuralLink < float >;
	template class CNeuralLink < int >;
};